/*
//Harjoitus 1
#include <iostream>
using namespace std;

int main() {

	//Aineen tiheyden tallentamiseen
	float tiheys;
	//Kuukauden numeron tallentamiseen
	short kknro;
	//Vuosiluvun tallentamiseen
	short vuosi;
	//Vokaalin tallentamiseen.
	char vokaali;

}
*/


//Harjoitus 2
/*
	Laske, kuinka suuria kokonaislukuja voisi tallentaa muuttujaan, jolle varataan tilaa 8 tavua.

	8*8 = 64
	2 potenssiin 64 = 18 446 744 073 709 551 616
	
*/

/*
//Harjoitus 3
#include <iostream>
using namespace std;

int main() {

	//Mik� arvo tulostuu ja miksi, kun seuraavat lauseet suoritetaan?
	unsigned char numero = 355;
	cout << numero;

	cin.get();
	return 0;


	//Arvo 'c' tulostuu, koska unsigned char-tietotyypin pituus on 256, numero-muuttujan arvo on 355, 
	//joten se k�y v�lin 0-256 uudestaan ylij��m�ll� arvolla: 355 - 256 = 99; 
	//ASCII: 99 = 'c'

}
*/

//Harjoitus 4
/*
	Mik� on merkin k ASCII-koodi? Ent� merkin K? Mik� on pienen ja ison kirjaimen erotus ASCII-koodistossa?

	k = 107
	K = 75
	107 - 75 = 32

*/

//Harjoitus 5
/*
	Mit� eroa on globaalilla ja funktion paikallisella muuttujalla ja kuinka ne esitell��n?

	Globaali muuttuja on alustettu ohjelmalohkojen ulkopuolella ja paikallinen muuttuja on alustettu ohjelmalohkon sis�ll�.
	Esimerkki:

	#include <iostream>
	using namespace std;

	int x = 55; // globaali muuttuja

	int main() {

		int i = 78; // paikallinen muuttuja

		cout << i << endl;
		cout << x << endl;

	}
*/
/*
//Harjoitus 7
int main() {

	//Laadi tietuerakenne, joka kuvaa suoritinta; mieti, millaisia 
	//ominaisuuksia suorittimissa on ja kehit� tietueeseen vastaavat kent�t.

	struct suoritin {
		short bit;
		float hinta;
	};

}
*/

/*
//Harjoitus 8
#include <iostream>
using namespace std;

int main() {

	//K�yt� harjoituksen 7 suoritintietuetta ohjelmassa. Luo 2 suoritinta ja sy�t� niiden tiedot ja tulosta ne.
	//Tee siis my�s 7. Suoritin = prosessori. Tietue on struct-rakenne.

	struct suoritin {
		short bit;
		float hinta;
	};

	suoritin srtn1;
	suoritin srtn2;

	srtn1.bit = 32;
	srtn1.hinta = 350;

	srtn2.bit = 16;
	srtn2.hinta = 225.6;

	cout << "Suoritin1 bit: " << srtn1.bit << endl;
	cout << "Suoritin1 hinta: " << srtn1.hinta << endl;

	cout << "Suoritin2 bit: " << srtn2.bit << endl;
	cout << "Suoritin2 hinta: " << srtn2.hinta << endl;

	cin.get();
	return 0;

}
*/
/*
//Harjoitus 9
int main() {

	//Laadi tietuerakenne, joka kuvaa suoritinta; mieti, millaisia 
	//ominaisuuksia suorittimissa on ja kehit� tietueeseen vastaavat kent�t.

	struct suoritin {
		short bit;
		float hinta;
	};

}
*/