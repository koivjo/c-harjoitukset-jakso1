/*
//Harjoitus 27
#include <iostream>
using namespace std;

int main() {

	//Ohjelma tulostaa parittomat luvut v�lilt� 0 - 20.

	for (int i = 0; i <= 20; i++) {
		if ((i % 2) != 0) cout << i << endl;
	};

	cin.get();
}
*/

/*
//Harjoitus 28
#include <iostream>
using namespace std;

int main() {

	//Ohjelma tulostaa parittomat luvut v�lilt� 0 - 90 paitsi kolmella jaolliset luvut.

	for (int i = 0; i <= 90; i++) {
		if (((i % 2) != 0) && ((i % 3) != 0)) cout << i << endl;
	};

	cin.get();
}
*/

/*
//Harjoitus 29
#include <iostream>
using namespace std;

int main() {

	//Ohjelma tulostaa sarjaa 1, 5, 25, 125, ..., kunnes sarjan j�senen arvo on yli 30000.

	int luku = 1;
	
	while (luku < 30000) {

		cout << luku << endl;
		luku = luku * 5;

	}

	cin.get();
}
*/

/*
//Harjoitus 35
#include <iostream>
using namespace std;

int main() {

	//Ohjelma generoi 5000 satunnaislukua v�lilt� 1-5 ja laskee kunkin numeron esiintymistaajuuden.

	int yksi = 0;
	int kaksi = 0;
	int kolme = 0;
	int nelja = 0;
	int viisi = 0;

	for (int i = 0; i <= 5000; i++) {
		
		int luku = rand() % 5 + 1;

		switch (luku) {
		case 1: yksi++; break;
		case 2: kaksi++; break;
		case 3: kolme++; break;
		case 4: nelja++; break;
		case 5: viisi++; break;
		default: break;
		}
	}

	cout << "Luvun 1 esiintymistaajuus: " << yksi << endl;
	cout << "Luvun 2 esiintymistaajuus: " << kaksi << endl;
	cout << "Luvun 3 esiintymistaajuus: " << kolme << endl;
	cout << "Luvun 4 esiintymistaajuus: " << nelja << endl;
	cout << "Luvun 5 esiintymistaajuus: " << viisi << endl;

	cin.get();
}
*/
