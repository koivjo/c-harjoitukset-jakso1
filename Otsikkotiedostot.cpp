// 39, 41
/*
//Harjoitus 39
#include <iostream>
#include <stdio.h>      //printf
#include <ctime>
using namespace std;

int main() {

	//Ohjelma tulostaa 10000 satunnaislukua v�lilt� 0 � 1000 ja laskee kuluneen ajan sekunteina.
	
	clock_t ajastin;
	double kesto;

	int luku;

	ajastin = clock();

	for (int i = 0; i <= 10000; i++) {
		luku = rand() % 1000;
		cout << luku << endl;
	}

	kesto = (clock() - ajastin) / (double)CLOCKS_PER_SEC;

	printf("Kesto sekunteina: %1.3fs", kesto);

	cin.get();
}
*/


//Harjoitus 41
#include <iostream>
#include <stdio.h>      /* printf */

using namespace std;

int main() {

	//Ohjelmassa annetaan sotu-tunnus merkkijonona (muodossa ppkkvv-nnnx). Ohjelma tulostaa syntym�ajan muodossa pp.kk.vv.

	char *nimi = "Koivula";
	
	char sotu[9];

	printf("Anna sotu(ppkkvv-xxxx): ");
	scanf_s("%s", sotu);

	printf(sotu);

	cin.get();
}